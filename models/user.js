var mongoose    = require("mongoose");
var userSchema  = new mongoose.Schema({
    name: String,
    username: {type: String, required: true, unique: true},
    password: {type: String, required: true},
    admin: Boolean,
    createdAt: Date,
    updatedAt: Date
});

module.exports   = mongoose.model("User", userSchema);