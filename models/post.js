var mongoose    = require("mongoose");
var postSchema  = new mongoose.Schema({
    title: String,
    post: String,
    author: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Post"
        }
    ]
});

module.exports = mongoose.model("Post", postSchema);