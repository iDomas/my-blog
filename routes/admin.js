var express     = require("express"),
    router      = express.Router();
var passport    = require("passport");
var User        = require("../models/user");

// main
router.get("/", function(req, res) {
    res.render("admin/home");
});

///login
router.get("/login", function(req, res) {
    res.render("admin/login");
});

// handle login
router.post("/login", passport.authenticate("local", {
    failureRedirect: "/admin/login"
}), function(req, res) {
    console.log("Signed in. as " + req.user.name + " | Date: " + new Date());
    res.redirect("/admin/");
});

// logout route
router.get("/logout", function(req, res) {
    req.logout();
    res.redirect("back");
});


module.exports = router;