var express         = require("express");
    router          = express.Router();
var Posts           = require("../models/post");



// BLOG MAIN ROUTE
router.get("/", function (req, res) {
    Posts.find({}, function (err, posts) {
        if (err) {
            console.log(err);
        } else {
            res.render("blog/home", {posts: posts});
        }
    });
});

router.get("/new", isCurrentUserAdmin, function (req, res) {
    res.render("blog/new");
});

router.post("/", isCurrentUserAdmin, function (req, res) {
    var data = req.body;
    Posts.create(data, function (err, post) {
       if (err) {
           console.log(err);
       }  else {
           res.redirect("/blog");
       }
    });
});

function isCurrentUserAdmin(req, res, next) {
    if (req.isAuthenticated()) {
        next();
    } else {
        res.render("admin/login");
    }
}

module.exports = router;