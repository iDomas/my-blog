var express     = require("express");
    router      = express.Router();

// MAIN APP ROUTE
router.get("/", function (req, res) {
    res.render("index");
});

module.exports = router;
