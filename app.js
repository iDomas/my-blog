var http                    = require("http"),
    express                 = require("express"),
    app                     = express(),
    mongoose                = require("mongoose"),
    bodyParser              = require("body-parser"),
    passport                = require("passport"),
    methodOverride          = require("method-override"),
    LocalStrategy           = require("passport-local").Strategy,
    User                    = require("./models/user"),
    Post                    = require("./models/post");

// require routes
var indexRoutes     = require("./routes/index");
var blogRoutes      = require("./routes/blog");
var adminRoutes     = require("./routes/admin");

// setup
mongoose.connect("mongodb://localhost/my-blog");
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static(__dirname + "/public"));
app.set("view engine", "ejs");
app.use(methodOverride("_method"));

// passport config
app.use(require("express-session")({
    secret: "Secret sentence asdfg 1234",
    resave: false,
    saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());
// set fields from db to use as authentication keys
passport.use(new LocalStrategy({
        usernameField: "username",
        passwordField: "password",
        session: false
    },
        function(username, password, cb) {
        User.findOne({username: username }, function (err, user) {
            if (err)    { return cb(err); }
            if (!user)  { return cb(null, false); }
            if (user.password !== password) { return cb(null, false); }
            return  cb(null, user);
        });
    }
));
passport.serializeUser(function(user, cb) {
    cb(null, user.id);
});
passport.deserializeUser(function(id, cb) {
    User.findById(id, function (err, user) {
        if (err) { return cb(err); }
        cb(null, user);
    });
});

// set currentUser global in project
app.use(function(req, res, next) {
    res.locals.currentUser = req.user;
    next();
});

// ROUTES
app.use(indexRoutes);
app.use("/admin", adminRoutes);
app.use("/blog", blogRoutes);


app.get("*", function (req, res) {
    res.send("404");
});

app.listen(1234, function() {
    console.log("My Blog started on 1234 port!");
});